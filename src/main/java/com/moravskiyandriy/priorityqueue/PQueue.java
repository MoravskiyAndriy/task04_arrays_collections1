package com.moravskiyandriy.priorityqueue;

import java.util.*;

public class PQueue<T> implements Queue<T> {
    List<T> list;
    Comparator<T> comparator;

    public PQueue(final Comparator<T> comparator) {
        list = new ArrayList<T>();
        this.comparator = comparator;
    }

    public boolean add(final T element) {
        try {
            list.add(element);
            Collections.sort(list, comparator);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean offer(final T element) throws ClassCastException, NullPointerException {
        try {
            list.add(element);
            Collections.sort(list, comparator);
            return true;
        } catch (Exception e) {
            if (Objects.isNull(element)) {
                throw new NullPointerException();
            } else {
                throw new ClassCastException();
            }
        }
    }

    public T remove() throws NoSuchElementException {
        T object;
        try {
            object = list.get(list.size() - 1);
            list.remove(object);
            return object;
        } catch (Exception e) {
            throw new NoSuchElementException();
        }
    }

    public boolean remove(final Object obj) throws NoSuchElementException {
        try {
            for (T element : list) {
                if (element.equals(obj)) {
                    list.remove(obj);
                }
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public T poll() {
        T object;
        try {
            object = list.get(list.size() - 1);
            list.remove(object);
            return object;
        } catch (Exception e) {
            return null;
        }
    }

    public T element() {
        return list.get(list.size() - 1);
    }

    public T peek() {
        try {
            return list.get(list.size() - 1);
        } catch (Exception e) {
            return null;
        }
    }

    public void clear() {
        list = new ArrayList<T>();
    }

    public Comparator comparator() {
        return this.comparator;
    }

    public boolean contains(Object o) {
        for (T el : list) {
            if (el.equals(o)) {
                return true;
            }
        }
        return false;
    }

    public boolean containsAll(final Collection<?> collection) {
        for (Object element : collection) {
            if (!list.contains((T) element)) {
                return false;
            }
        }
        return true;
    }

    public boolean addAll(final Collection<? extends T> collection) {
        try {
            for (Object element : collection) {
                list.add((T) element);
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean removeAll(final Collection<?> collection) {
        try {
            for (Object element : collection) {
                list.remove((T) element);
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean retainAll(final Collection<?> collection) {
        try {
            for (T element : list) {
                if (!collection.contains(element)) {
                    list.remove(element);
                }
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public Object[] toArray() {
        return list.toArray();
    }

    public <T> T[] toArray(final T[] a) throws NullPointerException {
        try {
            return list.toArray(a);
        } catch (Exception e) {
            throw new NullPointerException();
        }
    }

    public int size() {
        return list.size();
    }

    public boolean isEmpty() {
        if (list.size() == 0) {
            return true;
        }
        return false;
    }

    public Iterator iterator() {
        return list.iterator();
    }
}
