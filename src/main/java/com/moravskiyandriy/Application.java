package com.moravskiyandriy;

import com.moravskiyandriy.droid.BattleDroid;
import com.moravskiyandriy.droid.Droid;
import com.moravskiyandriy.generic.Ship;
import com.moravskiyandriy.priorityqueue.PQueue;
import com.moravskiyandriy.stringtointeger.WrongList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class Application {
    private static final Logger logger = LogManager.getLogger(Application.class);

    public static void main(String[] args) {
        logger.info("START\n");
        logger.info("For generic class:\n");
        Ship<Droid> ship = new Ship();
        ship.add(new Droid(20, 5));
        ship.add(new Droid(23, 6));
        ship.add(new Droid(25, 5));
        ship.add(new BattleDroid(28, 8, 6));
        ship.add(new BattleDroid(21, 5, 3));
        List<Droid> galera = ship.getDroids();
        ship.showDroids(galera);

        logger.info("\nFor custom PriorityQueue:");
        Queue<Droid> pqueue = new PQueue<>(Comparator.comparing(Droid::getHp).
                thenComparing((Droid::getAttack)));

        pqueue.addAll(galera);
        logger.info("\npqueue size= " + pqueue.size());

        while (!pqueue.isEmpty()) {
            logger.info("getting droid: " + pqueue.poll());
        }
        logger.info("pqueue size= " + pqueue.size());

        logger.info("\nFor 'Add String to List<Integer>':\n");
        List<Integer> badList = new ArrayList<>();
        WrongList.addToList(badList);
        for (int i = 0; i < badList.size(); i++) {
            logger.info(badList.get(i));
        }

        logger.info("\nEND");
    }
}
