package com.moravskiyandriy.generic;

import com.moravskiyandriy.droid.Droid;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class Ship<T extends Droid> {
    private static final Logger logger = LogManager.getLogger(Ship.class);
    List<T> ship;

    public Ship() {
        ship = new ArrayList<T>();
    }

    public void add(final T droid) {
        ship.add(droid);
    }

    public List<T> getDroids() {
        return ship;
    }

    public void showDroids(final List<? extends T> ship) {
        for (T droid : ship) {
            logger.info(droid);
        }
    }
}
