package com.moravskiyandriy.droid;

public class Droid {
    private int hp;
    private int attack;

    public Droid() {
    }

    public Droid(final int hp, final int attack) {
        this.hp = hp;
        this.attack = attack;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(final int hp) {
        this.hp = hp;
    }

    public int getAttack() {
        return attack;
    }

    public void setAttack(final int attack) {
        this.attack = attack;
    }

    @Override
    public String toString() {
        return "Droid{" +
                "hp=" + hp +
                ", attack=" + attack +
                '}';
    }
}
