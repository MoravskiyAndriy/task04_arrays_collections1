package com.moravskiyandriy.droid;

public class BattleDroid extends Droid {
    private int defence;

    public BattleDroid(final int hp, final int attack, final int defence) {
        super(hp, attack);
        this.defence = defence;
    }

    @Override
    public String toString() {
        return "BattleDroid{" +
                "hp=" + getHp() +
                ", attack=" + getAttack() +
                ", defence=" + defence +
                '}';
    }
}